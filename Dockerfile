FROM alpine
MAINTAINER Pijus Navickas <pijus.navickas@gmail.com>

EXPOSE 3000

RUN apk add --update nmap-ncat

ENTRYPOINT ["ncat", "-l", "3000", "-k", "-v", "-c", "xargs -n1 echo"]
